#!/bin/bash

 USAGE=" USAGE: sudo ./2-run_docker.sh policy_editor_tool 8005:8005"

if [ $# -eq 0 ]
  then
    echo $USAGE
    exit 1
fi

sudo docker run --name $1 -d -p $2 $1
