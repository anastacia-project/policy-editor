
/**
* @desc reset the specified input select
* @param jquery input select object
* @param option value
**/
function reset_select(jquery_select_object,object_type){
	jquery_select_object.find('option')
		.remove()
		.end()
		.append('<option value="" disabled="">Select '+object_type+'...</option>')
		.prop('selectedIndex',0)
		.prop('disabled', true);
}


function hspl_build(hspl_object){

	hspl_object = $(hspl_object)

	hspl_id = hspl_object.find('.policy-name').text()
	subject = hspl_object.find('.subject-select').val()
	action = hspl_object.find('.action-select').val()
	object = hspl_object.find('.object-select').val()
	//fields
	traffic_target_select = hspl_object.find('.traffic-target-select')
	purpose_select = hspl_object.find('.purpose-select')
	resource_select = hspl_object.find('.resource-select')

	if(!traffic_target_select.prop('disabled') && traffic_target_select.prop('selectedIndex')!=0)
		traffic_target = `<tns:traffic_target>
                  <tns:target_name>`+traffic_target_select.val()+`</tns:target_name>
        </tns:traffic_target>`
	else
		traffic_target = ""

	if (!purpose_select.prop('disabled') && purpose_select.prop('selectedIndex')!=0)
		purpose = `<tns:purpose>
                       <tns:purpose_property>
                  	       <tns:key>method</tns:key>
                  	       <tns:value>`+purpose_select.val()+`</tns:value>
                  	   </tns:purpose_property>
        </tns:purpose>`
	else
		purpose = ""

	if (!resource_select.prop('disabled') && resource_select.prop('selectedIndex')!=0)
		resource = `<tns:resource>
                   		<tns:resource_property>
                  	       <tns:key>`+resource_select.val()+`</tns:key>
                  	   </tns:resource_property>
        			</tns:resource>`
	else
		resource = ""


	if (traffic_target || purpose || resource)
		fields = `<tns:fields>
          `+traffic_target+purpose+resource+`
      </tns:fields>`
    else
    	fields = ""

	// dependencies
	dependencies = ""
	dependency_objs = hspl_object.find('.dependency')
	console.log(dependency_objs.length)
	if(dependency_objs.length > 0){
		dependency_objs.each(function(){
			dependency_type_select = getHSPLElement(this,".dependency-type-select")
			dependency_target_select = getHSPLElement(this,".dependency-target-select")
			if($(dependency_type_select).val() == "Policy"){
				dependencies+= `<tns:dependency xsi:type="tns:PolicyDependency">
            <tns:dependencyCondition xsi:type="tns:PolicyDependencyCondition">
              <tns:policyID>`+$(dependency_target_select).val()+`</tns:policyID>  
            <tns:status>ENFORCED</tns:status>
            </tns:dependencyCondition></tns:dependency>`
			}else{
				dependencies+=`   <tns:dependency xsi:type="tns:EventDependency">
            <tns:eventID>`+$(dependency_target_select).val()+`</tns:eventID>
        </tns:dependency> `
				
			}
		})
		dependencies= `<tns:dependencies>`+dependencies+`</tns:dependencies>`
	}

	// priority
	priority_value = hspl_object.find('.priority-select').val()
	priority = "<tns:priority>"+priority_value+"</tns:priority>"


	hspl = `<tns:hspl subject="`+subject+`" id="`+hspl_id+`" >
      <tns:action>`+action+`</tns:action>
      <tns:objectH>`+object+`</tns:objectH>
      `+fields+`
      `+dependencies+`
      `+priority+`
    </tns:hspl>`

	return hspl

}


function orchestration_hspl_build(){
	hspl_policies = []
	$('.hspl-policy').each(function(){
		hspl_policies.push(hspl_build(this))
	})

	orchestration_hspl = `<?xml version="1.0" encoding="UTF-8"?>
<tns:Mapping xmlns:tns="http://www.example.org/Refinement_Schema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.example.org/Refinement_Schema Refinement_Schema.xsd ">
  <tns:hspl_list xsi:type="tns:HSPL_Orchestration" id="X">
  	`+hspl_policies.join("\n")+`
  </tns:hspl_list>
</tns:Mapping>`
	console.log(orchestration_hspl)
	return orchestration_hspl
}

function show_loading(){
    $("#loadMe").modal({
      backdrop: "static", //remove ability to close modal with click
      keyboard: false, //remove option to close with keyboard
      show: true //Display loader!
    });
}

function hide_loading(){
	$("#loadMe").modal("hide");
}


function refine(hspl){
	// show loading
	show_loading()
	console.log("Sending")
	/*
	$.ajax({
	  type: "POST",
	  url: "h2mservice",
	  data: hspl,
	  dataType: "text",
	  success: function( data ) {
	    console.log( "Data Loaded: " + data );
	    //detect_conflicts(data) 
	    show_refinement(data)
	  }
	});
	*/

	$.post( url="h2mservice", hspl)
	  .done(function( data ) {
	    console.log( "Data Loaded: " + data );
	    //show_refinement(hspl,data)
	    detect_conflicts(hspl,data)
	})
	
}

function show_refinement(ohspl,omspl,notification_type){
	// Remove loading
	hide_loading()
	// Show the refinement
	$("#hspl-form-container").addClass("d-none");
	    $("#translate-container").removeClass("d-none");
	    $("#hspl_text").val(ohspl);
	    $("#mspl_text").val(omspl);
	    if(notification_type == "warn"){
	    	$("#mspl_text").css("background","#FFD0B0")
	    	$("#enforce-btn").removeClass("disabled")
	    }else if(notification_type == "error"){
	    	$("#mspl_text").css("background","#FFB0B0")
	    	$("#enforce-btn").addClass("disabled")
	    }else{
	    	$("#mspl_text").css("background","#C3FFB0")
	    	$("#enforce-btn").removeClass("disabled")
	    } 
}

function detect_conflicts(ohspl,omspl){
$.post( "mcdtservice", {post_data:omspl})
	.done(function( data ) {
	    console.log( "Result: " + data );
	    data = JSON.parse(data)
	    notification_type = "error"
	    if(!$.isEmptyObject(data["mspl_conflicts"])){
	    	//notification_type = "error"
	    	$.notify("Coflicts detected:"+data["mspl_conflicts"],notification_type)
	    }else if(!$.isEmptyObject(data["mspl_dependencies"])){
	    	notification_type = "warn"
	    	$.notify("Dependencies detected:"+data["mspl_dependencies"],notification_type)
	    }else{
	    	notification_type = "success"
	    	$.notify("Refinement Performed Successfully","success")
	    }
	   	show_refinement(ohspl,omspl,notification_type)
	})


	/*
	$.post( "mcdtservice", {post_data:omspl})
	  .done(function( data ) {
	    console.log( "Result: " + data );
	})*/	
}


function enforce(mspl_list){
	console.log(mspl_list)
	show_loading()
	//json_mspl_list = JSON.parse(mspl_list)
	$.post( "m2eservice", {post_data:mspl_list})
	  .done(function( data ) {
	    console.log( "Result: " + data );
	    // open policy table
	    window.location.replace("/status");
	}).fail(function( data ) {
	    console.log( "Result: " + data );
	    // show error notification
	    $.notify("An error ocurred during orchestration process","error")
	}).always(function( data ) {
	    console.log( "Result: " + data );
	    hide_loading()
	})
}

function generateHSPL(){
	HSPL_INDEX++
	new_hspl_policy = base_hspl_fields.clone()
	/*new_hspl_policy.find(".trash-container")
		.append('<button id="remove-hspl-'+
			HSPL_INDEX+
			'-btn" class="btn btn-primary float-right remove-hspl-btn" type="button"><i class="fa fa-trash"></i></button>')*/
	new_hspl_policy.find(".trash-container")
		.append('<button id="remove-hspl-'+
			HSPL_INDEX+
			'-btn" class="btn btn-primary remove-hspl-btn" type="button"><i class="fa fa-trash"></i></button>')
	new_hspl_policy_text = new_hspl_policy[0].outerHTML
	$("#hspl-policies-container").append(new_hspl_policy_text.replace(/-0/g,'-'+HSPL_INDEX))
}

function generateDependency(add_button){
	//policy_index = 
	//console.log($(add_button).closest("div.card-body","div.card-footer"))
	dependencies_container = $(add_button).parents("div.card-body").find("div.dependencies-container")
	new_dependency = base_dependency_fields.clone()
	new_dependency.removeClass("d-none")
	dependencies_container.append(new_dependency[0].outerHTML)
	//dependencies_container.append(new_dependency.html())
	//console.log(new_dependency)
	
}


function removeDependency(remove_button){
	container = $(remove_button).parents('div.card-container')
	$(remove_button).parents('div.dependency').remove()
	// If there is no remining dependencies, close the accordion
	if (!container.find('div.dependency').length)
		container.parent().find('a').click()	
}

function getHSPLElement(from,element_class){
	return $(from).parents('div.hspl-policy').find(element_class)
}



// On document ready
jQuery(document).ready(function(){

	// Clone the empty status of base dependency
	base_dependency_fields = $(".dependency").clone()

	// Counter of security policies
	HSPL_INDEX = 0

	// Get the actions
    $.get("get_actions", function(data, status){
	    //alert("Data: " + data + "\nStatus: " + status);
	    actions = data["actions"]
	    base_hspl_fields = $("#hspl-policy-0")
	    $.each(actions,function(index){
	    	op = $('<option value="'+actions[index]+'">'+actions[index]+'</option>');
			base_hspl_fields.find(".action-select").append(op);
	    	//$(".action-select").append(op);
	    });
	});
	// Clone the current status of the HSPL-0, including the actions
	base_hspl_fields = $("#hspl-policy-0").clone()

    // This must be from body in order to generate the change
    // On change action select
	$('body').on('change', ".action-select", function() {
		// Reset the rest of selects
		subject_select = getHSPLElement(this,".subject-select")
		object_select = getHSPLElement(this,".object-select")
		traffic_target_select = getHSPLElement(this,".traffic-target-select")
		purpose_select = getHSPLElement(this,".purpose-select")
		resource_select = getHSPLElement(this,".resource-select")

		// clear the previous status of the select
		reset_select(subject_select,"subject")
		reset_select(object_select,"object")
		reset_select(traffic_target_select,"traffic-target")
		reset_select(purpose_select,"purpose")
		reset_select(resource_select,"resource")

		// get the objects
		$.get("get_objects?action="+this.value, function(data, status){		    

		    // Load the objects
		    objects = data["objects"]
		    $.each(objects,function(index){
		    	op = $('<option value="'+objects[index]+'">'+objects[index]+'</option>');
		    	object_select.append(op);
		    });
		    // verify if must be disabled or not
		    if($(object_select).children('option').length>1){
		    	object_select.prop('disabled', false);
		    }else
		    	object_select.prop('disabled', true);
		});
	});

	// On change object select
	$('body').on('change', ".object-select", function() {
		//selected_action = $("#action-select").val()
		show_loading()
		selected_action = getHSPLElement(this,".action-select").val()
		that = this
		// Get the subjects
	    $.get("get_subjects?action="+selected_action+"&object="+this.value, function(data, status){
		    subjects = data["subjects"]
		    subject_select = getHSPLElement(that,".subject-select")
		    //console.log(that)
		    //console.log(subject_select)
		    reset_select(subject_select,"subject")
		    $.each(subjects,function(index){
		    	op = $('<option value="'+subjects[index]+'">'+subjects[index]+'</option>');
		    	subject_select.append(op);
		    });

		    // verify if must be disabled or not
		    if($(subject_select).children('option').length>1){
		    	subject_select.prop('disabled', false);
		    }else
		    	subject_select.prop('disabled', true);
		}).fail(function(){
			$.notify("Unable to retrieve system model information","error")
		}).always(function( data ) {
			// We need to delay the timeout since the loading takes time to load
			// so if the hide is executed while the loading in starting it does not work
		    setTimeout(hide_loading,500)
		});
		
		// get the traffic target
		$.get("get_fields?object="+this.value+"&action="+selected_action, function(data, status){
			console.log("get_traffic target")
			console.log(data["fields"])
			traffic_target = data["fields"]["traffic-target"]
			//1. update traffic-target
			//traffic_target_select = $("#traffic-target-select")
			traffic_target_select = getHSPLElement(that,".traffic-target-select")
		    // clear the previous status of the select
		    reset_select(traffic_target_select,"traffic-target")
		    // Load the objects
		    $.each(traffic_target,function(index){
		    	op = $('<option value="'+traffic_target[index]+'">'+traffic_target[index]+'</option>');
		    	traffic_target_select.append(op);
		    });
		    // verify if must be disabled or not
		    if($(traffic_target_select).children('option').length>1){
		    	traffic_target_select.prop('disabled', false);
		    }else
		    	traffic_target_select.prop('disabled', true);

			//2. update purpose
			purpose = data["fields"]["purpose"]
			//1. update traffic-target
			purpose_select = getHSPLElement(that,".purpose-select")
		    // clear the previous status of the select
		    reset_select(purpose_select,"purpose")
		    // Load the objects
		    $.each(purpose,function(index){
		    	op = $('<option value="'+purpose[index]+'">'+purpose[index]+'</option>');
		    	purpose_select.append(op);
		    });
		    // verify if must be disabled or not
		    if($(purpose_select).children('option').length>1){
		    	purpose_select.prop('disabled', false);
		    }else
		    	purpose_select.prop('disabled', true);


			//3. update resource
			resource = data["fields"]["resource"]
			//1. update traffic-target
			resource_select = getHSPLElement(that,".resource-select")
		    // clear the previous status of the select
		    reset_select(resource_select,"resource")
		    // Load the objects
		    $.each(resource,function(index){
		    	op = $('<option value="'+resource[index]+'">'+resource[index]+'</option>');
		    	resource_select.append(op);
		    });
		    // verify if must be disabled or not
		    if($(resource_select).children('option').length>1){
		    	resource_select.prop('disabled', false);
		    }else
		    	resource_select.prop('disabled', true);
		});
	});	

    // On change dependency-type select
	$('body').on('change', ".dependency-type-select", function() {
		dependency_target_select = getHSPLElement(this,".dependency-target-select")
		if($(this).val() == "Policy"){
			current_policy_fake_id = getHSPLElement(this,".policy-name").text()
			reset_select(dependency_target_select,"dependency target")
			// Get current security policies
			$('.hspl-policy').each(function(){
				hspl_fake_id =  $(this).find(".policy-name").text()
				if (hspl_fake_id != current_policy_fake_id){
					op = $('<option value="'+hspl_fake_id+'">'+hspl_fake_id+'</option>');
					dependency_target_select.append(op);
				}
			})
		}else if($(this).val() == "Event"){
			reset_select(dependency_target_select,"dependency target")
			dependency_target_select.append($('<option>AUTHN_SUCCESS</option>'));
			//dependency_target_select.prop('selectedIndex',0)
			//.prop('disabled', true);

		}
		// verify if must be disabled or not
		if($(dependency_target_select).children('option').length>1){
		 	dependency_target_select.prop('disabled', false);
		}else
		  	dependency_target_select.prop('disabled', true);

	});

	// On click add HSPL
	$("#add-hspl-btn").on('click', function(){
		generateHSPL();
	});

	// On remove hspl
	$('body').on('click', ".remove-hspl-btn", function() {
		$(this).parents('div.hspl-policy').remove()
	});

	// On add dependency
	$('body').on('click', ".add-dependency-btn", function() {
		generateDependency(this);
	});

	// On remove dependency
	$('body').on('click', ".remove-dependency-btn", function() {
		removeDependency(this)
	});

	// On refine button
	$("#refine-btn").on('click', function(){
		refine(orchestration_hspl_build())
	});

	// On enforce button
	$("#enforce-btn").on('click', function(){
		enforce($("#mspl_text").val())
	});

	// On enforce button
	$("#modify-btn").on('click', function(){
		$("#translate-container").addClass("d-none");
		$("#hspl-form-container").removeClass("d-none");
	});

	// On clear button
	$("#clear-btn").on('click', function(){
		$("#translate-container").addClass("d-none");
	    $("#hspl_text").val("");
	    $("#mspl_text").val("");
	    // change translate button by clear button
	    $("#translate-btn-container").removeClass("d-none");
	    $("#clear-btn-container").addClass("d-none");
	});

});