from django.conf.urls import url, include
from . import views

urlpatterns = [
	url(r'^$', views.EditorToolView.as_view(), name="editor"),
	url(r'^get_subjects$', views.Subjects.as_view(), name="get_subjects"),
	url(r'^get_actions$', views.Actions.as_view(), name="get_actions"),
	url(r'^get_objects$', views.Objects.as_view(), name="get_objects"),
	url(r'^get_fields$', views.Fields.as_view(), name="get_fields"),
	url(r'^h2mservice$', views.H2MService.as_view(), name="h2mservice"),
	url(r'^m2eservice$', views.M2EService.as_view(), name="m2eservice"),
	url(r'^mcdtservice$', views.MCDTService.as_view(), name="mcdtservice"),
]
