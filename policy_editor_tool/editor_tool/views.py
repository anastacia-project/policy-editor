# # -*- coding: utf-8 -*-
# views.py
"""
This python module implements the HSPL policy editor tool inside the ANASTACIA European Project,
extending the HSPL/MSPL language defined in secured project.
How to use:
	Request for editor tool: http://anastacia-framework:8005/editor/
"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"

from django.shortcuts import render
from django.views.generic import TemplateView
from rest_framework.views import APIView
from rest_framework.response import Response
#import copy
import requests
import json
import logging
import sysmodel
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
from policy_editor_tool.settings import POLICY_INTERPRETER_H2M_URL, POLICY_INTERPRETER_M2L_URL,\
 POLICY_INTERPRETER_CDT_URL, H2MSERVICE, M2ESERVICE, MCDTSERVICE, SM_USER, SM_PASS

# TODO: Retrieve this information from the System Model

# Actions
CONFIG_AUTHENTICATION = "config_authentication"
AUTHORISE_ACCESS = "authorise_access"
NO_AUTHORISE_ACCESS = "no_authorise_access"
PROT_CONF_INTEGR = "prot_conf_integr"
ENABLE = "enable"

# Objects
RESOURCE = "resource"
ALL_TRAFFIC = "AllTraffic"
PANA_TRAFFIC = "PANA_traffic"
PANA = "PANA"
DTLS_TRAFFIC = "DTLS_traffic"

# Traffic target
IOT_CONTROLLER = "IoT_Controller"
IOT_BROKER = "IoT_Broker"
PANA_AGENT = "PANA_agent"

# Purpose
PUT = "PUT"
PSK = "psk"

PEANA1 = "PEANA-1"
PEANA2 = "PEANA-2"
SENSOR2 = "Sensor-1"
SENSOR4 = "Sensor-2"

# Resource
TEMPERATURE = "TEMPERATURE"
HUMIDITY = "HUMIDITY"
POWER_OFF = "POWER_OFF"

RESOURCES = [POWER_OFF,TEMPERATURE,HUMIDITY]

ACTIONS = [CONFIG_AUTHENTICATION,AUTHORISE_ACCESS,NO_AUTHORISE_ACCESS,PROT_CONF_INTEGR,ENABLE]

DEVICES = [SENSOR2,SENSOR4]

ROOMS = [PEANA1,PEANA2]

SUBJECTS = DEVICES+ROOMS


BULK_ENABLED = {(AUTHORISE_ACCESS,PANA_TRAFFIC):1,
				(AUTHORISE_ACCESS,DTLS_TRAFFIC):1,
				(NO_AUTHORISE_ACCESS,PANA_TRAFFIC):1,
				(NO_AUTHORISE_ACCESS,DTLS_TRAFFIC):1}


# TODO: Load from json?
HSPL_MATRIX = {
					CONFIG_AUTHENTICATION:{
											"objects":{
												PANA:{
													"traffic-target":[PANA_AGENT],
													"purpose":[PSK],
													"resource":[],
													"subjects":DEVICES
												}
											}
											},
					AUTHORISE_ACCESS:{
									  "objects":{
									  		RESOURCE:{
												"traffic-target":[IOT_BROKER],
												"purpose":[PUT],
												"resource":[TEMPERATURE,HUMIDITY],
												"subjects":DEVICES
											},
											#ALL_TRAFFIC:{
											#	"traffic-target":[IOT_CONTROLLER,IOT_BROKER,PANA_AGENT],
											#	"purpose":[],
											#	"resource":[],
											#	"subjects":SUBJECTS
											#},
											PANA_TRAFFIC:{
												"traffic-target":[PANA_AGENT],
												"purpose":[],
												"resource":[],
												"subjects":SUBJECTS
											},
											DTLS_TRAFFIC:{
												"traffic-target":[IOT_BROKER,IOT_CONTROLLER],
												"purpose":[],
												"resource":[],
												"subjects":SUBJECTS
											},
										},
									},
					NO_AUTHORISE_ACCESS:{
									  "objects":{
									  		RESOURCE:{
												"traffic-target":[IOT_BROKER],
												"purpose":[PUT],
												"resource":[TEMPERATURE,HUMIDITY],
												"subjects":DEVICES
											},
											#ALL_TRAFFIC:{
											#	"traffic-target":[IOT_CONTROLLER,IOT_BROKER,PANA_AGENT],
											#	"purpose":[],
											#	"resource":[],
											#	"subjects":SUBJECTS
											#},
											PANA_TRAFFIC:{
												"traffic-target":[PANA_AGENT],
												"purpose":[],
												"resource":[],
												"subjects":SUBJECTS
											},
											DTLS_TRAFFIC:{
												"traffic-target":[IOT_BROKER,IOT_CONTROLLER],
												"purpose":[],
												"resource":[],
												"subjects":SUBJECTS
											},
										},
									},
					PROT_CONF_INTEGR:{
									  "objects":{
											ALL_TRAFFIC:{
												"traffic-target":[IOT_CONTROLLER,IOT_BROKER],
												"purpose":[],
												"resource":[],
												"subjects":[SENSOR2,SENSOR4]
											},
										},	
									},
					ENABLE:{
									  "objects":{
											RESOURCE:{
												"traffic-target":[],
												"purpose":[PUT],
												"resource":RESOURCES,
												"subjects":[SENSOR2,SENSOR4]
											},
										},
									},
				}
	

def basic_auth_api(api_instance):
	"""Basic system model auth"""
	api_instance.api_client.configuration.username = SM_USER
	api_instance.api_client.configuration.password = SM_PASS


class EditorToolView(TemplateView):
	template_name = 'editor_tool/editor_tool.html'


class Subjects(APIView):

	def custom_device_names(self, devices):
		api_instance = sysmodel.RoomApi()
		basic_auth_api(api_instance)
		device_names = []
		for device in devices.results:
			#room = api_instance.room_list(name=self.register_obj["name"])
			device_name = device.name
			if device.room:
				logger.info("Retrieving room information")
				room = api_instance.room_read(id=device.room)
				device_name = "{}#{}".format(room.description,device_name)
				logger.info("Device name is now: {}".format(device_name))
			device_names.append(device_name)
		return device_names


	def get_available_devices(self):
		api_instance = sysmodel.PNFApi()
		basic_auth_api(api_instance)
		devices = api_instance.p_nf_list(_request_timeout=1)
		# Custom device names by including the location
		device_names = self.custom_device_names(devices)
		#device_names = [device.name for device in devices.results]
		logger.info("received devices: {}".format(device_names))
		return device_names


	def get_available_rooms(self):
		api_instance = sysmodel.RoomApi()
		basic_auth_api(api_instance)
		rooms = api_instance.room_list()
		room_names = [room.name for room in rooms.results]
		logger.info("received rooms: {}".format(room_names))
		return room_names


	def get_available_subjects(self, action, received_object):
		# TODO: Order by room?
		#try:
		subjects = self.get_available_devices()
		# Verify bulk
		if BULK_ENABLED.get((action,received_object)):
			subjects=subjects+self.get_available_rooms()
		#logger.info("sorted list: {}".format(subjects.sorted()))
		subjects.sort()
		return subjects
		#except Exception as e:
		#	logger.error(e)
		#	subjects = HSPL_MATRIX[action]["objects"][received_object]["subjects"]





		"""
		api_instance = sysmodel.PNFApi()
		basic_auth_api(api_instance)
		try:
			devices = api_instance.p_nf_list()
			subjects = [device.name for device in devices.results]
			logger.info("received subjects: {}".format(subjects))
			# TODO: Also include rooms
			api_instance = sysmodel.RoomApi()
			basic_auth_api(api_instance)
			rooms = api_instance.room_list()
			subjects=subjects+[room.name for room in rooms.results]
			logger.info("received subjects: {}".format(subjects))

		except Exception as e:
			logger.error(e)
			subjects = SUBJECTS

		"""
		# TODO: Also include rooms

		# TODO: Recalculate the matrix
		return subjects 


	def get(self, request, format=None):
		# Get available subjects on system model
		try:
			action = request.query_params["action"]
			received_object = request.query_params["object"]
			# Uncomment this line in order to ignore remote system model (for local testing) 
			#return Response({'subjects':HSPL_MATRIX[action]["objects"][received_object]["subjects"]})
			subjects = self.get_available_subjects(action,received_object)
			# TODO: use the matrix instead return all subjects
			return Response({'subjects':subjects})
		except Exception as e:
			return Response(status=500)
			#return Response({'subjects':HSPL_MATRIX[action]["objects"][received_object]["subjects"]})

class Actions(APIView):

	def get(self, request, format=None):
		#return Response({'actions':ACTIONS})
		return Response({'actions':HSPL_MATRIX.keys()})

class Objects(APIView):

	def get(self, request, format=None):
		action = request.query_params["action"]
		#return Response({'objects':ACTION_OBJECT_MATRIX[action]})
		return Response({'objects':HSPL_MATRIX[action]["objects"].keys()})

class Fields(APIView):

	def get(self, request, format=None):
		action = request.query_params["action"]
		received_object = request.query_params["object"]
		#fields = copy.copy(OBJECT_FIELDS_MATRIX[received_object])
		#if action == REQUIRE_AUTHENTICATION:
		#	print("Match require authentication")
		#	fields["purpose"] = [PSK]
		#print(fields)
		#return Response({'fields':fields})
		#return Response({'fields':OBJECT_FIELDS_MATRIX[received_object]})
		return Response({'fields':HSPL_MATRIX[action]["objects"][received_object]})

class H2MService(APIView):

	def post(self, request, format=None):
		#action = request.query_params["action"]
		#xml = request.data
		xml = request.stream.read().decode("utf-8")
		#logger.info("Received in H2MEDITORAPI: {}".format(xml))
		logger.info("REQUESTING H2M REFINEMENT TO POLICY INTERPRETER {}".format(POLICY_INTERPRETER_H2M_URL))
		headers = {'Content-Type': 'application/xml'}
		response = requests.post("{}/{}".format(POLICY_INTERPRETER_H2M_URL,H2MSERVICE),data=xml,headers=headers)
		#logger.info("RECEIVED FROM THE POLICY INTERPRETER:")
		#logger.info(response.text)
		#fields = copy.copy(OBJECT_FIELDS_MATRIX[received_object])
		#if action == REQUIRE_AUTHENTICATION:
		#	print("Match require authentication")
		#	fields["purpose"] = [PSK]
		#print(fields)
		#return Response({'fields':fields})
		#return Response(json.dumps(json.loads(response.text)))
		return Response(response.text)

class M2EService(APIView):

	def post(self, request, format=None):
		#action = request.query_params["action"]
		body = request.data["post_data"]
		#print(body)
		#print(dir(body))
		#print(json.dumps(body))
		response = requests.post("{}/{}".format(POLICY_INTERPRETER_M2L_URL,M2ESERVICE),data=body,timeout=3600)
		#print(response.text)
		#fields = copy.copy(OBJECT_FIELDS_MATRIX[received_object])
		#if action == REQUIRE_AUTHENTICATION:
		#	print("Match require authentication")
		#	fields["purpose"] = [PSK]
		#print(fields)
		#return Response({'fields':fields})
		return Response(response.text)

class MCDTService(APIView):

	def post(self, request, format=None):
		#action = request.query_params["action"]
		body = request.data["post_data"]
		#logger.info("EDITOR_TOOL_MCDTService")
		#logger.info("test")
		#logger.info("Received {}".format(body))
		#print(body)
		#print(dir(body))
		#print(json.dumps(body))
		headers = {'Content-Type': 'application/xml'}
		response = requests.post("{}/{}".format(POLICY_INTERPRETER_CDT_URL,MCDTSERVICE),data=body,headers=headers,timeout=3600)
		#print(response.text)
		#fields = copy.copy(OBJECT_FIELDS_MATRIX[received_object])
		#if action == REQUIRE_AUTHENTICATION:
		#	print("Match require authentication")
		#	fields["purpose"] = [PSK]
		#print(fields)
		#return Response({'fields':fields})
		return Response(response.text)