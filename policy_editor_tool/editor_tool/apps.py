from django.apps import AppConfig


class EditorToolConfig(AppConfig):
    name = 'editor_tool'
