# VNFRetrieve

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**interfaces** | [**list[Interface]**](Interface.md) |  | [optional] 
**type** | **str** |  | 
**state** | **str** |  | 
**name** | **str** |  | 
**tag** | **str** |  | 
**model** | **str** |  | 
**description** | **str** |  | 
**alias** | **str** |  | 
**flavor** | **str** |  | 
**rt_resource** | **int** |  | [optional] 
**softwares** | **list[int]** |  | [optional] 
**cloud** | **list[int]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


