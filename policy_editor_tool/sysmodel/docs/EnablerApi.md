# swagger_client.EnablerApi

All URIs are relative to *http://195.148.125.100:9000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**enabler_create**](EnablerApi.md#enabler_create) | **POST** /enabler/ | 
[**enabler_delete**](EnablerApi.md#enabler_delete) | **DELETE** /enabler/{id}/ | 
[**enabler_list**](EnablerApi.md#enabler_list) | **GET** /enabler/ | 
[**enabler_partial_update**](EnablerApi.md#enabler_partial_update) | **PATCH** /enabler/{id}/ | 
[**enabler_read**](EnablerApi.md#enabler_read) | **GET** /enabler/{id}/ | 
[**enabler_update**](EnablerApi.md#enabler_update) | **PUT** /enabler/{id}/ | 


# **enabler_create**
> Enabler enabler_create(data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.EnablerApi(swagger_client.ApiClient(configuration))
data = swagger_client.Enabler() # Enabler | 

try:
    api_response = api_instance.enabler_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnablerApi->enabler_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Enabler**](Enabler.md)|  | 

### Return type

[**Enabler**](Enabler.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **enabler_delete**
> enabler_delete(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.EnablerApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique value identifying this enabler.

try:
    api_instance.enabler_delete(id)
except ApiException as e:
    print("Exception when calling EnablerApi->enabler_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique value identifying this enabler. | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **enabler_list**
> InlineResponse2005 enabler_list(page=page)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.EnablerApi(swagger_client.ApiClient(configuration))
page = 56 # int | A page number within the paginated result set. (optional)

try:
    api_response = api_instance.enabler_list(page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnablerApi->enabler_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| A page number within the paginated result set. | [optional] 

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **enabler_partial_update**
> Enabler enabler_partial_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.EnablerApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique value identifying this enabler.
data = swagger_client.Enabler() # Enabler | 

try:
    api_response = api_instance.enabler_partial_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnablerApi->enabler_partial_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique value identifying this enabler. | 
 **data** | [**Enabler**](Enabler.md)|  | 

### Return type

[**Enabler**](Enabler.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **enabler_read**
> Enabler enabler_read(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.EnablerApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique value identifying this enabler.

try:
    api_response = api_instance.enabler_read(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnablerApi->enabler_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique value identifying this enabler. | 

### Return type

[**Enabler**](Enabler.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **enabler_update**
> Enabler enabler_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.EnablerApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique value identifying this enabler.
data = swagger_client.Enabler() # Enabler | 

try:
    api_response = api_instance.enabler_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnablerApi->enabler_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique value identifying this enabler. | 
 **data** | [**Enabler**](Enabler.md)|  | 

### Return type

[**Enabler**](Enabler.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

