# LoWPANInterface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**virtual** | **bool** |  | [optional] 
**type** | **str** |  | 
**name** | **str** |  | 
**pan_id** | **str** |  | 
**assigned_mac** | **str** |  | 
**device** | **int** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


