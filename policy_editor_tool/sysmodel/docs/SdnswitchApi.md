# swagger_client.SdnswitchApi

All URIs are relative to *http://195.148.125.100:9000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sdnswitch_create**](SdnswitchApi.md#sdnswitch_create) | **POST** /sdnswitch/ | 
[**sdnswitch_delete**](SdnswitchApi.md#sdnswitch_delete) | **DELETE** /sdnswitch/{id}/ | 
[**sdnswitch_list**](SdnswitchApi.md#sdnswitch_list) | **GET** /sdnswitch/ | 
[**sdnswitch_partial_update**](SdnswitchApi.md#sdnswitch_partial_update) | **PATCH** /sdnswitch/{id}/ | 
[**sdnswitch_read**](SdnswitchApi.md#sdnswitch_read) | **GET** /sdnswitch/{id}/ | 
[**sdnswitch_update**](SdnswitchApi.md#sdnswitch_update) | **PUT** /sdnswitch/{id}/ | 


# **sdnswitch_create**
> SDNSwitch sdnswitch_create(data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.SdnswitchApi(swagger_client.ApiClient(configuration))
data = swagger_client.SDNSwitch() # SDNSwitch | 

try:
    api_response = api_instance.sdnswitch_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SdnswitchApi->sdnswitch_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**SDNSwitch**](SDNSwitch.md)|  | 

### Return type

[**SDNSwitch**](SDNSwitch.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **sdnswitch_delete**
> sdnswitch_delete(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.SdnswitchApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this sdn switch.

try:
    api_instance.sdnswitch_delete(id)
except ApiException as e:
    print("Exception when calling SdnswitchApi->sdnswitch_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this sdn switch. | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **sdnswitch_list**
> InlineResponse20014 sdnswitch_list(page=page)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.SdnswitchApi(swagger_client.ApiClient(configuration))
page = 56 # int | A page number within the paginated result set. (optional)

try:
    api_response = api_instance.sdnswitch_list(page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SdnswitchApi->sdnswitch_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| A page number within the paginated result set. | [optional] 

### Return type

[**InlineResponse20014**](InlineResponse20014.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **sdnswitch_partial_update**
> SDNSwitch sdnswitch_partial_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.SdnswitchApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this sdn switch.
data = swagger_client.SDNSwitch() # SDNSwitch | 

try:
    api_response = api_instance.sdnswitch_partial_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SdnswitchApi->sdnswitch_partial_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this sdn switch. | 
 **data** | [**SDNSwitch**](SDNSwitch.md)|  | 

### Return type

[**SDNSwitch**](SDNSwitch.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **sdnswitch_read**
> SDNSwitch sdnswitch_read(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.SdnswitchApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this sdn switch.

try:
    api_response = api_instance.sdnswitch_read(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SdnswitchApi->sdnswitch_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this sdn switch. | 

### Return type

[**SDNSwitch**](SDNSwitch.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **sdnswitch_update**
> SDNSwitch sdnswitch_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.SdnswitchApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this sdn switch.
data = swagger_client.SDNSwitch() # SDNSwitch | 

try:
    api_response = api_instance.sdnswitch_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SdnswitchApi->sdnswitch_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this sdn switch. | 
 **data** | [**SDNSwitch**](SDNSwitch.md)|  | 

### Return type

[**SDNSwitch**](SDNSwitch.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

