# coding: utf-8

"""
    Anastacia System Model

    Anastacia system model development version  # noqa: E501

    OpenAPI spec version: dev
    Contact: mohammed.boukhalfa@aato.fi
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.api.flavor_api import FlavorApi  # noqa: E501
from swagger_client.rest import ApiException


class TestFlavorApi(unittest.TestCase):
    """FlavorApi unit test stubs"""

    def setUp(self):
        self.api = swagger_client.api.flavor_api.FlavorApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_flavor_create(self):
        """Test case for flavor_create

        """
        pass

    def test_flavor_delete(self):
        """Test case for flavor_delete

        """
        pass

    def test_flavor_list(self):
        """Test case for flavor_list

        """
        pass

    def test_flavor_partial_update(self):
        """Test case for flavor_partial_update

        """
        pass

    def test_flavor_read(self):
        """Test case for flavor_read

        """
        pass

    def test_flavor_update(self):
        """Test case for flavor_update

        """
        pass


if __name__ == '__main__':
    unittest.main()
