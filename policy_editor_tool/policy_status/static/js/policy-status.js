
/*
function show_loading(){
    $("#loadMe").modal({
      backdrop: "static", //remove ability to close modal with click
      keyboard: false, //remove option to close with keyboard
      show: true //Display loader!
    });
}

function hide_loading(){
	$("#loadMe").modal("hide");
}
*/

// On document ready
jQuery(document).ready(function(){
	//show_loading()
	var table_element = $('#status-table')
	var url = table_element.attr("url")

	table_element.dataTable( {
	        "ajax": {
	            "url": url,
	            "type": "GET"
	        },
	        "columns": [
	            { "data": "ID" },
	            { "data": "type" },
	            { "data": "capability" },
	            { "data": "creation_date" },
	            { "data": "status" },
	            { "targets": -1, "data": null, "defaultContent": '<button class="btn"><i class="fa fa-trash"></i></button>'}
	        ],
	        "order": [3, "desc"],
	        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
	        	/*
	        	var status = {"P":{"status":"warning","text":"Pending"},
					        	"E":{"status":"success","text":"Enforced"},
					        	"D":{"status":"danger","text":"Removed"}
					        	}
				*/
				var status_label = {"success":{"label":"success","text":"Success"},
								"pending":{"label":"warning","text":"Pending"},
					        	"failed":{"label":"danger","text":"Failed"}
				}
	        	console.log(aData)
	        	console.log(aData["status"])
	        	console.log(status_label[aData["status"]]["label"])
	        	// Modify label according on the status
	        	$("td:eq(4)", nRow).html('<span class="badge badge-'+status_label[aData["status"]]["label"]+'">'+status_label[aData["status"]]["text"]+'</span>')
	        	console.log(aData["status"])
	        	// Add remove button
	        	$("td:eq(5)", nRow).html('<button class="btn"><i class="fa fa-trash"></i></button>')
	        	
		  	},
		  	//"initComplete": function(settings, json) {
			//    hide_loading()
			//  }
	    } );

	// Delete a record
    table_element.on('click', 'a.editor_remove', function (e) {
        e.preventDefault();
 
        editor.remove( $(this).closest('tr'), {
            title: 'Delete record',
            message: 'Are you sure you wish to remove this record?',
            buttons: 'Delete'
        } );
    } );


});