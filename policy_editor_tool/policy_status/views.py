from django.shortcuts import render
from django.views.generic import TemplateView
from rest_framework.views import APIView
from rest_framework.response import Response
import requests
import json
import logging
import sysmodel
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
from policy_editor_tool.settings import SM_USER, SM_PASS


POLICY_REPOSITORY_URL = "http://policy-repository.es:8004/api/v1/"
POLICY_REPOSITORY_MSPL_URL = "mspl-instances"
POLICY_RESPOSITORY_STATUS = "policy-enforcements"


def basic_auth_api(api_instance):
	"""Basic system model auth"""
	api_instance.api_client.configuration.username = SM_USER
	api_instance.api_client.configuration.password = SM_PASS


class PolicyStatusView(TemplateView):
	template_name = 'policy_status/policy_status.html'

class StatusTableView(APIView):


	def get_policy_enforcement_list(self):
		api_instance = sysmodel.PolicyenforcementApi()
		basic_auth_api(api_instance)
		policy_enforcement_list = api_instance.policyenforcement_list()
		
		return policy_enforcement_list


	def get(self, request, format=None):
		policy_enforcement_list = self.get_policy_enforcement_list()
		logger.debug("received policy enforcement list: {}".format(policy_enforcement_list))
		data_table = []
		for policy_enforcement in policy_enforcement_list.results:
			mspl_text = policy_enforcement.mspl
			data_table.append({
				'ID':policy_enforcement.id,
				'type': "MSPL",
				'capability':mspl_text.split('<Name>')[1].split('</Name')[0],
				'creation_date':policy_enforcement.update.strftime('%Y-%m-%d %H:%M:%S'),
				'status': policy_enforcement.status
				})
		return Response({"data":data_table})


	"""
	def get(self, request, format=None):
		response = requests.get("{}{}".format(POLICY_REPOSITORY_URL,POLICY_RESPOSITORY_STATUS))
		policy_status = json.loads(response.text)
		data_table = []
		# Get status
		for mspl in policy_status:

			
			print(mspl)
			mspl_text = mspl["mspl"]["mspl"]
			print(mspl_text)
			data_table.append({"ID":mspl_text.split('ID="')[1].split('"')[0],
								"type":"MSPL",
								"capability":mspl_text.split('<Name>')[1].split('</Name>')[0],
								"creation_date":mspl["creation_date"],
								"status":mspl["status"]})

		print(data_table)
			
		return Response({"data":data_table})
	"""