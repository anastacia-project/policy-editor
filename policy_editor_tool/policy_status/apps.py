from django.apps import AppConfig


class PolicyStatusConfig(AppConfig):
    name = 'policy_status'
