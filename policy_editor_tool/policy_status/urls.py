from django.conf.urls import url, include
from . import views

urlpatterns = [
	url(r'^$', views.PolicyStatusView.as_view(), name="policy_status"),
	url(r'^status_table$', views.StatusTableView.as_view(), name="status_table"),
]
